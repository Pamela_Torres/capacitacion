<?php

namespace App;

use App\Course;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    //
     protected $fillable=[

		'nombre_mod',
		
	];


	public function course()
	{


		return $this->hasMany(Course::class);
	}
}
