<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relator extends Model
{
    //
    protected $fillable = [
      'rut_rel',
      'nombre_rel',
      'apellido_pat_rel',
      'apellido_mat_rel',
      'correo_rel',
      'telefono_rel',
    ];




    public function course()
	{


		return $this->hasMany(Course::class);
	}


	// public function scopeRelator($query, $rut_rel)
 //    {
 //        if ($rut_rel) 
 //            return $query->orwhere('rut_rel','LIKE',"%$rut_rel%");
 //            # code...
 //        }


	
}

//              "nombre_rel"=>'required',
//              "apellido_pat_rel"=>'required',
//              "apellido_mat_rel"=>'required',
//              "correo_rel"=>'required',
//              "telefono_rel"=>'required',
