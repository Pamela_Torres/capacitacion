<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //
    protected $fillable = [
      'carreer_id','rut_doc','nombre_doc','apellido_pat_doc','apellido_mat_doc','correo_doc','telefono_doc'
    ];
}
