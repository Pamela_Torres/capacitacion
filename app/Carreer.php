<?php

namespace App;

use App\institution;
use Illuminate\Database\Eloquent\Model;

class Carreer extends Model
{
    //
     protected $fillable=[

			
			'institution_id',
			'nombre_car',
		];

		
		public function institution()
		{
			return $this->belongsTo(Institution::class);

		}
}
