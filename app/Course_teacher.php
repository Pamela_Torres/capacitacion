<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_teacher extends Model
{
    //
     protected $fillable=[

		'course_id',
		'teacher_id',
		
	];

	public function course()
	{
		return $this->belongsTo(Course::class);

	}

	public function teacher()
	{
		return $this->belongsTo(Teacher::class);
    
}
}


