<?php

namespace App;

use App\Module;
use App\Relator;
use App\Schedule;
use App\Teacher;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Course extends Model
{
    //
    protected $fillable=[

		'module_id',
		'relator_id',
		'schedule_id',
		'nombre_cur',
		'modalidad_cur',
		'duracion_cur',
		'sala_cur',
	];


//listo de aqi

	public function module()
	{
		return $this->belongsTo(Module::class);

	}

	public function relator()
	{
		return $this->belongsTo(Relator::class);

	}

	public function schedule()
	{
		return $this->belongsTo(Schedule::class);

	}
 //hasta

	public function teacher()
	{
		return $this->belongsToMany(Teacher::class);

	}


	//query scope

	public function scopeSearch($query, $nombre_cur)
	{
	$query ->orwhere('nombre_cur','LIKE',"%$nombre_cur%");
    }

	


	
}
