<?php

namespace App\Http\Controllers;

use App\Relator;
use Illuminate\Http\Request;

class RelatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



        $relators = Relator::latest()->paginate(5);
  
        return view('relators.index',compact('relators'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('relators.create');
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            

             "rut_rel"=>'required',
             "nombre_rel"=>'required',
             "apellido_pat_rel"=>'required',
             "apellido_mat_rel"=>'required',
             "correo_rel"=>'required',
             "telefono_rel"=>'required',

        ]);
  
        Relator::create($request->all());
   
        return redirect()->route('relators.index')
                        ->with('success','Relator creado con exito.');
    }
   
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Relator $relator)
    {
        return view('relators.show',compact('relator'));
        
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Relator $relator)
    {
        return view('relators.edit',compact('relator'));
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Relator $relator)
    {
        $request->validate([
             'rut_rel'=>'required',
             'nombre_rel'=>'required',
             'apellido_pat_rel'=>'required',
             'apellido_mat_rel'=>'required',
             'correo_rel'=>'required',
             'telefono_rel'=>'required',

        ]);
  
        $relator->update($request->all());
  
        return redirect()->route('relators.index')
                        ->with('success','Relator updated successfully');
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Relator $relator)
    {
        $relator->delete();
  
        return redirect()->route('relators.index')
                        ->with('success','Relator deleted successfully');
    }


    //query scope

    
}
