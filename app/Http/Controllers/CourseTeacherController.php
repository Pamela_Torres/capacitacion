<?php

namespace App\Http\Controllers;

use App\Course_teacher;
use Illuminate\Http\Request;

class CourseTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $course_teachers = Course_teacher::latest()->paginate(5);
  
        return view('course_teachers.index',compact('course_teachers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('course_teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            

             "course_id"=>'required',
             "teacher_id"=>'required',
            

        ]);
  
        Course_teacher::create($request->all());
   
        return redirect()->route('course_teachers.index')
                        ->with('success',' creado course_teachers exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\course_teacher  $course_teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Course_teacher $course_teacher)
    {
        //
        return view('course_teachers.show',compact('course_teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\course_teacher  $course_teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Course_teacher $course_teacher)
    {
        //
        return view('course_teachers.edit',compact('course_teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\course_teacher  $course_teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course_teacher $course_teacher)
    {
        //
        $request->validate([
             'course_id'=>'required',
             'teacher_id'=>'required',
             

        ]);
  
        $course_teacher->update($request->all());
  
        return redirect()->route('course_teachers.index')
                        ->with('success','course_teachers updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\course_teacher  $course_teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course_teacher $course_teacher)
    {
        //
         $course_teacher->delete();
  
        return redirect()->route('course_teachers.index')
                        ->with('success','course_teachers deleted successfully');
    }
}
