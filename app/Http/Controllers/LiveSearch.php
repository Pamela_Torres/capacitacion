<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Relator;

class LiveSearch extends Controller
{
    function index()
    {
     return view('live_search');
    }

    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
        
       $data = DB::table('relators')
         ->orwhere('rut_rel', 'like', '%'.$query.'%')
         ->orWhere('nombre_rel', 'like', '%'.$query.'%')
         ->orWhere('apellido_pat_rel', 'like', '%'.$query.'%')
         ->orWhere('apellido_mat_rel', 'like', '%'.$query.'%')
         ->orWhere('correo_rel', 'like', '%'.$query.'%')
         ->orderBy('id', 'desc')
         ->get();

      //  'rut_rel',
      // 'nombre_rel',
      // 'apellido_pat_rel',
      // 'apellido_mat_rel',
      // 'correo_rel',
      // 'telefono_rel',
         
      }
      else
      {
       $data = DB::table('relators')
         ->orderBy('id', 'desc')
         ->get();
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $output .= '
        <tr>
         <td>'.$row->rut_rel.'</td>
         <td>'.$row->nombre_rel.'</td>
         <td>'.$row->apellido_pat_rel.'</td>
         <td>'.$row->apellido_mat_rel.'</td>
         <td>'.$row->correo_rel.'</td>
         <td>'.$row->telefono_rel.'</td>
        </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
     }
    }
}
