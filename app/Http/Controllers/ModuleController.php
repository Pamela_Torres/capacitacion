<?php

namespace App\Http\Controllers;

use App\Module;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $modules = Module::latest()->paginate(5);
  
        return view('modules.index',compact('modules'))
            ->with('i', (request()->input('page', 1) - 1) * 5);



             $data = Module::with('courses')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            

             "nombre_mod"=>'required',
           

        ]);
  
        Module::create($request->all());
   
        return redirect()->route('modules.index')
                        ->with('success','Modulo creado con exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        return view('modules.show',compact('module'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
         return view('modules.edit',compact('module'));
    }

    /**
     * Update the specified resource in storage.                                                      
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
        $request->validate([
             'nombre_mod'=>'required',
             

        ]);
  
        $module->update($request->all());
  
        return redirect()->route('modules.index')
                        ->with('success','Module updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
        //
        $module->delete();
  
        return redirect()->route('modules.index')
                        ->with('success','Module deleted successfully');
    }
}
