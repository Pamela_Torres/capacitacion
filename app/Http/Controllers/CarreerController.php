<?php

namespace App\Http\Controllers;

use App\Carreer;
use Illuminate\Http\Request;

class CarreerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $carreers = Carreer::latest()->paginate(5);
  
        return view('carreers.index',compact('carreers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return view('carreers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            

             "institution_id"=>'required',
             "nombre_car"=>'required',
             
        ]);
  
        Carreer::create($request->all());
   
        return redirect()->route('carreers.index')
                        ->with('success','Carreer creado con exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carreer  $carreer
     * @return \Illuminate\Http\Response
     */
    public function show(Carreer $carreer)
    {
        //
         return view('carreers.show',compact('carreer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carreer  $carreer
     * @return \Illuminate\Http\Response
     */
    public function edit(Carreer $carreer)
    {
        //
         return view('carreers.edit',compact('carreer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carreer  $carreer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carreer $carreer)
    {
        //
         $request->validate([
             'institution_id'=>'required',
             'nombre_car'=>'required',
             

        ]);
  
        $carreer->update($request->all());
  
        return redirect()->route('carreers.index')
                        ->with('success','carreer updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carreer  $carreer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carreer $carreer)
    {
        //
         $carreer->delete();
  
        return redirect()->route('carreers.index')
                        ->with('success','carreer deleted successfully');
    }
}
