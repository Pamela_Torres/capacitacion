<?php

namespace App\Http\Controllers;

use App\Course;
use App\Schedule;
use App\Module;
use App\Relator;
use App\Search_course;
use Illuminate\Http\Request;

class SearchCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
         $courses = Course::orderBy('id','desc')->paginate(5);
         $courses->each(function($courses){
                 $courses->module;
                 $courses->relator;
                 $courses->schedule;

                 dd($courses);

        });

         return view('courses.index')
         ->with('courses',$courses);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Search_course  $search_course
     * @return \Illuminate\Http\Response
     */
    public function show(Search_course $search_course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Search_course  $search_course
     * @return \Illuminate\Http\Response
     */
    public function edit(Search_course $search_course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Search_course  $search_course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Search_course $search_course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Search_course  $search_course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Search_course $search_course)
    {
        //
    }
}
