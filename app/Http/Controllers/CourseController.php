<?php

namespace App\Http\Controllers;

use App\Course;
use App\Module;
use App\Relator;
use App\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //$modules = Module::all();
        //return view('courses.index',compact('modules'));
     //   $modalidad_cur = $request->get('modalidad_cur');



    // $courses = DB::table('courses as course')
    
    // ->join('modules as module', 'module.id','=','course.module_id')
    // ->join('relators as relator', 'relator.id','=','course.relator_id')
    // ->join('schedules as schedule', 'schedule.id','=','course.schedule_id')
    // ->select('course.id','module.nombre_mod','relator.nombre_rel','schedule.fecha_hor','course.nombre_cur','course.modalidad_cur','course.duracion_cur','course.sala_cur')
    // ->get();


   // return view('courses.index',["courses"=>$courses]);

   $courses =Course::Search($request->nombre_cur)->orderBy('id','DESC')->paginate(5);
   $courses->each(function($courses){
    $courses->relator;
    $courses->modules;
    $courses->schedule;
   });

   return view('courses.index')
   ->with('courses',$courses);









      //Course::findOefail($id);


       

        // $courses = Course::latest()->paginate(5);
        // // //  $modules = Module::latest()->paginate(5);
  
        //   return view('courses.index',compact('courses'))
        //       ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            

             "module_id"=>'required',
             "relator_id"=>'required',
             "schedule_id"=>'required',
             "nombre_cur"=>'required',
             "modalidad_cur"=>'required',
             "duracion_cur"=>'required',
             "sala_cur"=>'required',

        ]);
  
        Course::create($request->all());
   
        return redirect()->route('courses.index')
                        ->with('success','Curso creado con exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
        return view('courses.show',compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
        return view('courses.edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
         $request->validate([          
             "module_id"=>'required',
             "relator_id"=>'required',
             "schedule_id"=>'required',
             "nombre_cur"=>'required',
             "modalidad_cur"=>'required',
             "duracion_cur"=>'required',
             "sala_cur"=>'required',

        ]);
  
        $course->update($request->all());
  
        return redirect()->route('courses.index')
                        ->with('success','Curso updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
          $course->delete();
  
        return redirect()->route('courses.index')
                        ->with('success','Curso deleted successfully');
    }




}
