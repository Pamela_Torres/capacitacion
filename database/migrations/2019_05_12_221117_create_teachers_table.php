<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('carreer_id');
            $table->string('rut_doc');
            $table->string('nombre_doc');
            $table->string('apellido_pat_doc');
            $table->string('apellido_mat_doc');
            $table->string('correo_doc');
            $table->string('telefono_doc');
            $table->timestamps();

            $table->foreign('carreer_id')->references('id')->on('carreers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
