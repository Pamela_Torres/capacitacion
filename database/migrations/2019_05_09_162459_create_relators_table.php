<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relators', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->string('rut_rel');
            $table->string('nombre_rel');
            $table->string('apellido_pat_rel');
            $table->string('apellido_mat_rel');
            $table->string('correo_rel');
            $table->string('telefono_rel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relators');
    }
}
