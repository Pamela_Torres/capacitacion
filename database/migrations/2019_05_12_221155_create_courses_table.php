<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('module_id');
            $table->unsignedBigInteger('relator_id');
            $table->unsignedBigInteger('schedule_id');
            $table->string('nombre_cur');
            $table->string('modalidad_cur');
            $table->string('duracion_cur');
            $table->string('sala_cur');
            $table->timestamps();


            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('relator_id')->references('id')->on('relators');
            $table->foreign('schedule_id')->references('id')->on('schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
