<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', function () {
    return view('index');
 });

Route::resource('relators','RelatorController');
Route::resource('modules','ModuleController');
Route::resource('schedules','ScheduleController');
Route::resource('courses','CourseController');
Route::resource('institutions','InstitutionController');
Route::resource('carreers','CarreerController');
Route::resource('teachers','TeacherController');
Route::resource('course_teachers','CourseTeacherController');
//Route::resource('courses','SearchCourseController');
//Route::get('/live_search','LiveSearch@index');
//Route::get('/live_search/action','LiveSearch@action')->name('live_search.action');



//Route::get('/live_search', 'LiveSearch@index');
//Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');

//Route::get('/live_search', 'RelatorController@index')->name('relators');

Route::get('/live_search', 'LiveSearch@index');
Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');

Route::get('/live_search2', 'LiveSearch2@index');
Route::get('/live_search2/action', 'LiveSearch2@action')->name('live_search2.action');



