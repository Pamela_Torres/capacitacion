@extends('relators.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Relators</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('relators.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('relators.update',$relator->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Rut:</strong>
                    <input type="text" name="rut_rel" value="{{ $relator->rut_rel}}" class="form-control" placeholder="rut_rel">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>nombre:</strong>
                    <input type="text" name="nombre_rel" value="{{ $relator->nombre_rel}}" class="form-control" placeholder="nombre_rel">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido Paterno:</strong>
                    <input type="text" name="apellido_pat_rel" value="{{ $relator->apellido_pat_rel }}" class="form-control" placeholder="apellido_pat_rel">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido Materno:</strong>
                    <input type="text" name="apellido_mat_rel" value="{{ $relator->apellido_mat_rel}}" class="form-control" placeholder="apellido_mat_rel">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Correo:</strong>
                    <input type="text" name="correo_rel" value="{{ $relator->correo_rel }}" class="form-control" placeholder="correo_rel">
                </div>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Telefono:</strong>
                    <input type="text" name="telefono_rel" value="{{ $relator->telefono_rel }}" class="form-control" placeholder="telefono_rel">
                </div>
            </div>

            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection