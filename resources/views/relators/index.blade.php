@extends('relators.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('relators.create') }}"> Crear nuevo relator</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


   
    <table class="table">
        <tr>
            <th>No</th>
            <th>Rut</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Correo</th>
            <th>Telefono</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($relators as $relator)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $relator->rut_rel}}</td>
            <td>{{ $relator->nombre_rel}}</td>
            <td>{{ $relator->apellido_pat_rel}}</td>
            <td>{{ $relator->apellido_mat_rel}}</td>
            <td>{{ $relator->correo_rel}}</td>
            <td>{{ $relator->telefono_rel}}</td>
            <td>
                <form action="{{ route('relators.destroy',$relator->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('relators.show',$relator->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('relators.edit',$relator->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $relators->links() !!}
      
@endsection