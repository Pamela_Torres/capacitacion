@extends('schedules.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('schedules.create') }}"> Crear nuevo Horario</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($schedules as $schedule)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $schedule->fecha_hor}}</td>
            <td>{{ $schedule->hora_hor}}</td>
            
            <td>
                <form action="{{ route('schedules.destroy',$schedule->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('schedules.show',$schedule->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('schedules.edit',$schedule->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $schedules->links() !!}
      
@endsection