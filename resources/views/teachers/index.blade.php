@extends('teachers.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('teachers.create') }}"> Crear nuevo teacher</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Rut</th>
            <th>carreer_id</th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Correo</th>
            <th>Telefono</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($teachers as $teacher)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $teacher->carreer_id}}</td>
            <td>{{ $teacher->rut_doc}}</td>
            <td>{{ $teacher->nombre_doc}}</td>
            <td>{{ $teacher->apellido_pat_doc}}</td>
            <td>{{ $teacher->apellido_mat_doc}}</td>
            <td>{{ $teacher->correo_doc}}</td>
            <td>{{ $teacher->telefono_doc}}</td>
            <td>
                <form action="{{ route('teachers.destroy',$teacher->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('teachers.show',$teacher->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('teachers.edit',$teacher->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $teachers->links() !!}
      
@endsection