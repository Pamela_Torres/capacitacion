@extends('teachers.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Relators</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('teachers.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('teachers.update',$teacher->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>carreer_id:</strong>
                    <input type="text" name="carreer_id" value="{{ $teacher->carreer_id}}" class="form-control" placeholder="carreer_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Rut:</strong>
                    <input type="text" name="rut_doc" value="{{ $teacher->rut_doc}}" class="form-control" placeholder="rut_doc">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>nombre:</strong>
                    <input type="text" name="nombre_doc" value="{{ $teacher->nombre_doc}}" class="form-control" placeholder="nombre_doc">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido Paterno:</strong>
                    <input type="text" name="apellido_pat_doc" value="{{ $teacher->apellido_pat_doc }}" class="form-control" placeholder="apellido_pat_doc">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Apellido Materno:</strong>
                    <input type="text" name="apellido_mat_doc" value="{{ $teacher->apellido_mat_doc}}" class="form-control" placeholder="apellido_mat_doc">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Correo:</strong>
                    <input type="text" name="correo_doc" value="{{ $teacher->correo_doc }}" class="form-control" placeholder="correo_doc">
                </div>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Telefono:</strong>
                    <input type="text" name="telefono_doc" value="{{ $teacher->telefono_doc }}" class="form-control" placeholder="telefono_doc">
                </div>
            </div>

            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection