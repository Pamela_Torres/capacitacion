@extends('modules.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('modules.create') }}"> Crear nuevo modulo</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre Modulo</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($modules as $module)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $module->nombre_mod}}</td>
            <td>
                <form action="{{ route('modules.destroy',$module->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('modules.show',$module->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('modules.edit',$module->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $modules->links() !!}
      
@endsection