@extends('institutions.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('institutions.create') }}"> Crear nueva institution</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre_ins</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($institutions as $institution)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $institution->nombre_ins}}</td>
            <td>
                <form action="{{ route('institutions.destroy',$institution->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('institutions.show',$institution->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('institutions.edit',$institution->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $institutions->links() !!}
      
@endsection