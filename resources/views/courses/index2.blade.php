<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>


	<table class="table table-striped">
		<thead>
			<th>id</th>
			<th>module_id</th>
			<th>relator_id</th>
			<th>schedule_id</th>
			<th>nombre_cur</th>
			<th>modalidad_cur</th>
			<th>duracion_cur</th>
			<th>sala_cur</th>
		</thead>
		<tbody>
			@foreach($courses as $course)
			   <tr>
			   	<td>{{ $course->id }}</td>
			   	<td>{{ $course->module_id }}</td>
			   	<td>{{ $course->relator_id}}</td>
			   	<td>{{ $course->schedule_id }}</td>
			   	<td>{{ $course->nombre_cur }}</td>
			   	<td>{{ $course->modalidad_cur }}</td>
			   	<td>{{ $course->duracion_cur }}</td>
			   	<td>{{ $course->sala_cur }}</td>
		        <td>
		        	<a href="{{ route('courses.edit',$course->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

		        	<a href="{{ route('courses.destroy',$course->id) }}" onclick="return confirm('¿Seguro que desaeas aliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
		        	



		        </td>
		       </tr>
		</tbody>
		</body>
	</table>


</html>