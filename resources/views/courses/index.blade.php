
@extends('layout')
 
@section('content')


<table class="table table-striped">

    <a href="{{ route('courses.create') }}" class="btn btn-info">Registrar nuevo curso</a>

<!--buscador de courses-->

{!! Form::open(['route' =>'courses.index','method' =>'GET','class' =>'navbar-form pull-right'])!!}
    <div class="input-group">
    {!!Form::text('nombre_cur',null,['class' => 'form-control','placeholder' => 'Buscar curso..','aria-describedby' => 'search']) !!}
    <span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
    </div>
    {!! Form::close()!!}

<!--fin de buscador-->


    <hr>
        <thead>
            <th>id</th>
            <th>module_id</th>
            <th>relator_id</th>
            <th>schedule_id</th>
            <th>nombre_cur</th>
            <th>modalidad_cur</th>
            <th>duracion_cur</th>
            <th>sala_cur</th>
        </thead>
        <tbody>
            @foreach($courses as $course)
               <tr>
                <td>{{ $course->id }}</td>
                <td>{{ $course->module->nombre_mod }}</td>
                <td>{{ $course->relator->nombre_rel}}</td>
                <td>{{ $course->schedule->fecha_hor }}</td>
                <td>{{ $course->nombre_cur }}</td>
                <td>{{ $course->modalidad_cur }}</td>
                <td>{{ $course->duracion_cur }}</td>
                <td>{{ $course->sala_cur }}</td>
                <td>
                    <a href="{{ route('courses.edit',$course->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                    <a href="{{ route('courses.destroy',$course->id) }}" onclick="return confirm('¿Seguro que desaeas aliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                    



                </td>
               </tr>
               @endforeach
        </tbody>
       
    </table>



{{-- <form class="navbar-form navbar-left" role="search" action="{{url('courses.scopeSearch')}}">
 <div class="form-group">
  <input type="text" class="form-control" name='search' placeholder="Buscar ..." />
 </div>
 <button type="submit" class="btn btn-default">Buscar</button>
</form>

    


    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Cursos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('courses.create') }}"> Crear nuevo Curso</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            
            <th>Modulo</th>
            <th>Relator</th>
            <th>Horario</th>
            <th>Nombre Curso</th>
            <th>Modalidad</th>
            <th>Duracion</th>
            <th>sala</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($courses as $course )


        <tr>
            
            <td>{{ $course->nombre_mod}}</td>
            <td>{{ $course->nombre_rel}}</td>
            <td>{{ $course->fecha_hor}}</td>
            <td>{{ $course->nombre_cur}}</td>
            <td>{{ $course->modalidad_cur}}</td>
            <td>{{ $course->duracion_cur}}</td>
            <td>{{ $course->sala_cur}}</td>
            <td>
                <form action="{{ route('courses.destroy',$course->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('courses.show',$course->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('courses.edit',$course->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table> --}}
  
    {{-- {!! $courses->links() !!} --}}
      
@endsection