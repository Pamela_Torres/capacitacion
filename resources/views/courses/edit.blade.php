@extends('courses.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Course</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('courses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('courses.update',$course->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Module:</strong>
                    <input type="text" name="module_id" value="{{ $course->module_id}}" class="form-control" placeholder="module_id">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Relator:</strong>
                    <input type="text" name="relator_id" value="{{ $course->relator_id}}" class="form-control" placeholder="relator_id">
                </div>
            </div>  

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Horarios:</strong>
                    <input type="text" name="schedule_id" value="{{ $course->schedule_id}}" class="form-control" placeholder="schedule_id">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="text" name="nombre_cur" value="{{ $course->nombre_cur}}" class="form-control" placeholder="nombre_cur">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Modalidad:</strong>
                    <input type="text" name="modalidad_cur" value="{{ $course->modalidad_cur}}" class="form-control" placeholder="modalidad_cur">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Duracion:</strong>
                    <input type="text" name="duracion_cur" value="{{ $course->duracion_cur}}" class="form-control" placeholder="duracion_cur">
                </div>
            </div>
           <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>sala:</strong>
                    <input type="text" name="sala_cur" value="{{ $course->sala_cur}}" class="form-control" placeholder="sala_cur">
                </div>
            </div>

            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection