@extends('courses.layout')
  
@section('content')


<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Agregar Nuevo Docente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('courses.index') }}">Volver</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('courses.store') }}" method="POST">
    @csrf
  
     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>module_id:</strong>
                <input type="text" name="module_id" class="form-control" placeholder="module_id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>relator_id:</strong>
                <input type="text" name="relator_id" class="form-control" placeholder="relator_id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>schedule_id:</strong>
                <input type="text" name="schedule_id" class="form-control" placeholder="schedule_id">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>nombre_cur:</strong>
                <input type="text" name="nombre_cur" class="form-control" placeholder="nombre_cur">
            </div>
        </div>
        

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>modalidad_cur:</strong>
                <input type="text" name="modalidad_cur" class="form-control" placeholder="modalidad_cur">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Duracion:</strong>
                <input type="text" name="duracion_cur" class="form-control" placeholder="duracion_cur">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>sala_cur:</strong>
                <input type="text" name="sala_cur" class="form-control" placeholder="sala_cur">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Subir</button>
        </div>
    </div>
    </div>
   
</form>
@endsection