@extends('carreers.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Course</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('carreers.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('carreers.update',$carreer->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>institution_id:</strong>
                    <input type="text" name="institution_id" value="{{ $carreer->institution_id}}" class="form-control" placeholder="institution_id">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>nombre_ins:</strong>
                    <input type="text" name="nombre_car" value="{{ $carreer->nombre_car}}" class="form-control" placeholder="nombre_car">
                </div>
            </div>  

            

            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

   
    </form>
@endsection