@extends('carreers.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('carreers.create') }}"> Crear nuevo carreers</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>institution_id</th>
            <th>nombre_car</th>
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($carreers as $carreer)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $carreer->institution_id}}</td>
            <td>{{ $carreer->nombre_car}}</td>
            <td>
                <form action="{{ route('carreers.destroy',$carreer->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('carreers.show',$carreer->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('carreers.edit',$carreer->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $carreers->links() !!}
      
@endsection