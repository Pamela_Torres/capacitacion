@extends('course_teachers.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Course</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('course_teachers.index') }}">Volver</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>course:</strong>
                {{ $course_teacher->course_id }}
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>teacher:</strong>
                {{ $course_teacher->teacher_id }}
            </div>
        </div>
         

        
        
    </div>
@endsection