@extends('layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Capacitación Docente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('course_teachers.create') }}"> Crear nuevo Curso_teacher</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>curso</th>
            <th>teacher</th>
            
            <th width="280px">Acciones</th>
        </tr>
        @foreach ($course_teachers as $course_teacher)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $course_teacher->course_id}}</td> 
            <td>{{ $course_teacher->teacher_id}}</td>
           
            <td>
                <form action="{{ route('course_teachers.destroy',$course_teacher->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('course_teachers.show',$course_teacher->id) }}">Mostrar</a>
    
                    <a class="btn btn-primary" href="{{ route('course_teachers.edit',$course_teacher->id) }}">Editar</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $course_teachers->links() !!}

    
      
@endsection