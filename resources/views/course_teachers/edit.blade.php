@extends('course_teachers.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Course_teacher</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('course_teachers.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('course_teachers.update',$course_teacher->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>course_id:</strong>
                    <input type="text" name="course_id" value="{{ $course_teacher->course_id}}" class="form-control" placeholder="course_id">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>teacher:</strong>
                    <input type="text" name="teacher_id" value="{{ $course_teacher->teacher_id}}" class="form-control" placeholder="teacher_id">
                </div>
            </div>  

            

            

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection